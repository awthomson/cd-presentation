# CI/CD Automation
## ES Delivery Pipeline Automation

---

# Introduction

--

## The Software Stack
![alt text](resources/Prezzo-Images-Stack.png)

--

## Terminology

**Continuous Integration:**
> Development being done and tested alongside other development in progress

**Continuous Delivery:**
> A focus on getting changes delivered quickly and safely in a sustainable way

--

## What that means for us...

* Feedback to developers needs to be as fast as possible (<5 minutes)
* Environment consistency is critical
* Testing becomes integral part of pipeline

---

# Our Pipeline

--

## Reference Process Flow

<img src="resources/Process-Flow.png" data-canonical-src="resources/Process-Flow.png" width="800" height="500" />

--

## Reference Data Flow

<img src="resources/Prezzo-Images-Page-3.png" data-canonical-src="resources//Prezzo-Images-Page-3.png" width="600" height="500"/>

--

## Our Process Flow

<img src="resources/Prezzo-Images-Page-4.png" data-canonical-src="resources//Prezzo-Images-Page-4.png" width="600" height="500" />

--

## Our Process Flow (With Tools)

<img src="resources/Prezzo-Images-Page-5.png" data-canonical-src="resources//Prezzo-Images-Page-5.png" width="600" height="500" />

---

# Testing

--

## Test Tools

![Testing Tools](resources/AutoTesting.png)

--

## Extent Reports

<img src="resources/ExtentReports.png" data-canonical-src="resources/ExtentReports.png" width="900" height="500" />

---

# Metrics

--

## Example Metrics (Apigee Proxies)

From pushing code to validated in staging: **<20 minutes**
* 10 seconds build
* 90 seconds config (dev)
* 120 seconds deploy & validation
* 180 seconds smoketest
* 90 seconds config (staging)
* 120 seconds deploy & validation
* 180 seconds smoketest
* 300 seconds functional and regression testing

--

## Example Metrics (Apigee Proxies)

Deployment to production: **<5 minutes**
* 90 seconds config
* 30 seconds deploy
* 90 seconds deployment validation


---

## Challenges

* Developers mindset change (away from long-lived branches)
* Automated testing developed alongside code
* Need consistency from vendors, and certain standards to be followed
* Deployment windows fortnightly
* Automated tests need to be maintained
* Very few projects in ES space at the moment nicely fit the model
* Ongoing ownership of test suite (and the results)

---